package indexer;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;

import java.sql.ResultSet;


/**
* The IndexFiles program creates documents from a given result set. The 
* column names of the result set are set as the field names in the document. Each document 
* corresponds to one record in the MySQLdatabse. The document is created and indexed to solr 
* in this program.
*
* @author  Information Retrieval Group 18
* @version 1.0
* @since   2014-04-14 
*/
public class IndexFiles
{
    
    private static String url = "http://localhost:8983/solr";
    private static HttpSolrServer solrCore;
    private int brandWeight=2; //standard boost already given to all documents is 1
    private int likesCountWeight=2;
    private int commentCountWeight=1;
    
    /**
     * Creates an instance of solr 
     * 
     * @throws MalformedURLException
     */
    public IndexFiles() throws MalformedURLException
    {
         solrCore = new HttpSolrServer(url);
    }

    /**
     * Takes an SQL ResultSet and adds the documents to solr. 
     * 
     * @param rs A ResultSet from the database.
     * @throws SQLException
     * @throws SolrServerException
     * @throws IOException
     */
    public void addResultSet(ResultSet rs) throws SQLException,
            SolrServerException, IOException
    {
        int innerCount = 0;
        Collection<SolrInputDocument> docList = new ArrayList<SolrInputDocument>();
        ResultSetMetaData rsm = rs.getMetaData();
        int numColumns = rsm.getColumnCount();
        String[] colNames = new String[numColumns + 1];

        /**
         * JDBC numbers the columns starting at 1, so the normal java convention
         * of starting at zero won't work.
         */
        for (int i = 1; i < (numColumns + 1); i++)
        {
            colNames[i] = rsm.getColumnName(i);           
        }

        while (rs.next())
        {
            //count++;
            innerCount++;
            SolrInputDocument doc = new SolrInputDocument();
            String userName=rs.getString("userName");
            int commentCount=Integer.parseInt(rs.getString("commentCount"));
            int likesCount=Integer.parseInt(rs.getString("likesCount"));
            
            int popularityValue=(commentCount*commentCountWeight)+(likesCount*likesCountWeight);
            ArrayList<String> brandList = new ArrayList<String>(
            	    Arrays.asList("forever21","charleskeithofficial","hm","bershkacollection","gap","abercrombieandfitch_","aeropostale","levis","elleusa","zarausa","burberry","americanapparelusa"));

            for (int j = 1; j < (numColumns + 1); j++)
            {
                if (colNames[j] != null)
                {
                    Object f;
                    switch (rsm.getColumnType(j))
                    {
                        case Types.BIGINT:
                        {
                            f = rs.getLong(j);
                            break;
                        }
                        case Types.INTEGER:
                        {
                            f = rs.getInt(j);
                            break;
                        }
                        case Types.DATE:
                        {
                            f = rs.getDate(j);
                            break;
                        }
                        case Types.FLOAT:
                        {
                            f = rs.getFloat(j);
                            break;
                        }
                        case Types.DOUBLE:
                        {
                            f = rs.getDouble(j);
                            break;
                        }
                        case Types.TIME:
                        {
                            f = rs.getDate(j);
                            break;
                        }
                        case Types.BOOLEAN:
                        {
                            f = rs.getBoolean(j);
                            break;
                        }
                        default:
                        {
                            f = rs.getString(j);
                        }
                    }
                    
                    
                    
                    
                    //Index time ranking
                    if( colNames[j].equals("boostScore")){
                    	//if username is in our userList(important brands), boost score to 2                   	
                    	String format = "%1$07d";
                    	
                    	
                    	if(brandList.contains(userName)){	  
                    		int brandValue=(int)(brandWeight*popularityValue);
                        	String totalValue=String.format(format,brandValue);    	                    	
                        	doc.addField(colNames[j], totalValue);
	                    }
                    	else{
                        	String totalValue=String.format(format, popularityValue);
                        	doc.addField(colNames[j], totalValue);
                        }
                    	
                    }
                    else{
                    	
                    	doc.addField(colNames[j], f);
                    }
                    
                    
            }
          }
         //Add document to collection
         docList.add(doc); 
        }
        //Add the collection to solr
        solrCore.add(docList);
        //Commit changes to solr
        solrCore.commit();
        System.out.println(innerCount+" Documents added to collection.");
    }
    
    /**
     * This is the main method which calls the readDataBase() method from the MySQLAccess class
     * and passes the obtained resultSet to the addResultSet(ResultSet rs) method.  
     * 
     * @param args Command line arguments
     */
   
    public static void main(String[] args) {
  	  try {  		  
  		MySQLAccess s= new MySQLAccess();
  	  	IndexFiles indexSqlRecords=new IndexFiles();
  	  	ResultSet rs;
  		rs=s.readDataBase();
  		indexSqlRecords.addResultSet(rs); 
  		
  		s.close();
  	  } catch (Exception e) {
  		 e.getMessage();
  		 e.printStackTrace();
  	}  
  }
}