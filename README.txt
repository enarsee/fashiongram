README


********************Group Details**************************

Group ID-18

Member Details
1. Chemburkar Nishant Rajeev-------------------U1120929B
2. Sukheja Pooja Beju------------------------- U1122589K
3. Sukheja Prathna Beju------------------------U1122591K
4. Rakhi Singh --------------------------------U1122667L

******************Website Link***************************
http://enarsee.com/fashiongram

******************Solr Admin Link on Cloud Server***********************
http://54.187.40.103:8983/solr

******************Android App***************************
http://enarsee.com/fashiongram/android.apk

******************Link to youtube video***********************
http://youtu.be/g-6wrDb4a7U

The Web Application link provided above, and the Android are fully functional and tested. 
If you want to go through the steps and reproduce them, you can follow the instructions below

************Detailed instructions to execute crawling:*********************
1.	Set up database using mysql(host='localhost',user='root',passwd='wampmysql').
2.	Name the database 'instagram_data'
3.	Create a table 'jsondata' using the structure mentioned in crawling documentation. 'id' should be made primary key. 'lowresImage' should be made unique i.e to avoid duplication.
4.	This table is necessary to temporarily store crawled data.
5.	As long there is an internet connection and Instagram is up and working, run ntuirprojectgroup18>Crawling>Crawler>fetchDatav1.0.py to crawl. It will crawl Instagram api and store results in the table.
6.	Errors might be displayed during the crawling process. This is caught in the exception, this might be due to duplicate images or if the text contains non-ascii characters. This data is skipped and not stored.
7.	While running it will print item_count and row_count, item_count represents the number of items it crawled and row_count represents the number of items it actually stores.

**************Detailed Instructions to perform indexing:****************** 
1. Install Bitnami Solr.
2. Start solr from solr panel. 
3. The solr panel should have numDocs and maxDocs 0. 
3. Ensure that the MySQLdatabase exists and is running with the dataset. 
4. Open command line. 
5. Paste the indexer.jar from ntuirprojectgroup18>Indexing>indexer.jar in your preferred directory. The folder ntuirprojectgroup18>Indexing>Indexer contains the source code of indexer.jar. You can view and run it using a Java IDE.
6. Open command line , shift to the directory which contains indexer.jar, and execute the jar file by typing java -jar indexer.jar
7. Check solr, the crawled documents should be indexed on solr. 

********************Detailed instructions on deploying and setting up Solr on a Cloud Machine*******************
1. Acquire a machine instance from a web host, in our case, Amazon Web Services.
2. Set it up to have a Windows Server Operating system running.
3. Download Bitnami Stack for Solr from the Bitnami Website (https://bitnami.com/stack/solr)
4. Install Solr with the default configurations
5. Download Bitnami Stack for WAMP from Bitnami Website (https://bitnami.com/stack/wamp)
6. Install WAMP with the listening port for Apache different from the ones set up for Solr (We set up 82 and 444)
7. Download and install Python 2.7 from the python website (https://www.python.org/)
8. Update the Solr schema from ntuirprojectgroup18>Solr>schema.xml to C:\Bitnami\solr-4.7.x\apache-solr\solr\collection1\conf
9. Run the Python file at ntuirprojectgroup18>Crawling>Crawler>fetchData1.0.py
10. This will populate the mySQL database with the data
11. Paste the indexer.jar from ntuirprojectgroup18>Indexing>indexer.jar in your preferred directory.
12. Open command line , shift to the directory which contains indexer.jar, and execute the jar file by typing java -jar indexer.jar
13. Solr should now be accessible at localhost:8983/solr or the domain-name:8983/solr
14. Take note that, most Servers will block incoming connections via Firewalls, so make sure to set a rule to allow incoming connections on 8983,22,80 and 82.


****************Detailed Instructions to deploy web application*****************
The Web application is live at http://enarsee.com/fashiongram, however if you want to set it up manually, follow the steps below:
1. Install Apache, or any other web server capable of running PHP
2. Copy the files from the ntuirprojectgroup18>Web App to the running directory of the server (either "www" or "htdocs" or "public_html")
3. Once the Apache server is on, you should be able to access it by navigating to localhost
4. Recommended Browser is Google Chrome, and a Resolution of 1366x768 pixels

**************Detailed Instructions to deploy Android Application****************
1. You can download the Android Application APK at http://enarsee.com/fashiongram/android.apk
2. After copying the apk, to and Android device, the application can be installed by tapping it.
3. If you want to view the Android source, it is available at ntuirprojectgroup18>Android.
4. The code can be built using Android Studio or Eclipse with the ADT plugin 