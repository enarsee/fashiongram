package com.fashiongram.app;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Iterator;

public class MainActivity extends ActionBarActivity {

    public final static String EXTRA_MESSAGE = "com.example.webapitutorial.MESSAGE";
    public int start = 0;
    public int max;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_about) {
            LinearLayout images = (LinearLayout) findViewById(R.id.linear);
            images.removeAllViews();
            TextView aboutUs = new TextView(this);

            aboutUs.setText("FashionGram\r\nApp created for CZ4034\r\n\r\nCrawls Instagram for specific photos\r\nIndexes and ranks in a Solr-based system\r\nView web app at www.enarsee.com/fashiongram\r\n\r\nCreated by Nishant, Pooja, Prathna and Rakhi");
            images.addView(aboutUs);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    public void searchFG(View view) {

        executeQuery();


    }

    public void executeQuery(){
        EditText query  = (EditText) findViewById(R.id.query);
        String queryText = query.getText().toString();
        if(start==0)
        {
        InputMethodManager imm = (InputMethodManager) getSystemService(MainActivity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }
        LinearLayout images = (LinearLayout) findViewById(R.id.linear);
        images.removeAllViews();

        String serverIp = "http://54.187.40.103:8983/solr/collection1/select?";



        if( queryText != null && !queryText.isEmpty()) {

            String queryParam = "wt=json&start="+start+"&rows=5&sort=boostScore%20desc%2ClikesCount%20desc%2CcaptionTime%20desc%2CcommentCount%20desc&q=tags:*"+queryText+"*OR+captionText:*"+queryText+"*";

            CallAPI async = new CallAPI(this);
            async.execute(serverIp+queryParam);

        }
    }

    public void nextPage()
    {
        if((start+6)<=max )
        {
            start = start+6;
            executeQuery();
        }
    }

    public void previousPage()
    {
        if((start-6)>=0 )
        {
            start = start-6;
            executeQuery();
        }
    }

    private class UpdateUI implements Runnable{

        public JSONObject doc;
        @Override
        public void run() {
            LinearLayout images = (LinearLayout) findViewById(R.id.linear);

            LinearLayout imageHolder = new LinearLayout(MainActivity.this);
            imageHolder.setOrientation(1);
            TextView userName = new TextView(MainActivity.this);
            TextView caption = new TextView(MainActivity.this);

            ImageView image = new ImageView(MainActivity.this);
            Bitmap a = null;
            try {
                a = (Bitmap)doc.get("stanresImage");
                image.setImageBitmap(a);
                userName.setText(doc.getString("userName"));
                userName.setTextSize(12);
                caption.setText(doc.getString("captionText"));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            imageHolder.addView(userName);
            imageHolder.addView(caption);
           imageHolder.addView(image);

            imageHolder.setPadding(0,25,0,25);

            images.addView(imageHolder);

        }
    }

    private class displayMessage implements Runnable{

        public String message;
        @Override
        public void run() {
            LinearLayout images = (LinearLayout) findViewById(R.id.linear);
            images.removeAllViews();
            TextView aboutUs = new TextView(MainActivity.this);

            aboutUs.setText(message);
            images.addView(aboutUs);

        }
    }

    private class CallAPI extends AsyncTask<String, String, String> {

        private Context mContext;
        public CallAPI (Context context){
            mContext = context;
        }

        ProgressDialog pd;

        @Override
        protected String doInBackground(String... params) {

            String urlString=params[0];

            String resultToDisplay = "";

            InputStream is = null;
            String result = "";
            JSONObject jsonObject = null;

            // HTTP
            try {
                HttpClient httpclient = new DefaultHttpClient(); // for port 80 requests!
                HttpGet httppost = new HttpGet(urlString);
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
            } catch(Exception e) {
                return null;
            }

            // Read response to string
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"),8);
                StringBuilder sb = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                is.close();
                result = sb.toString();
            } catch(Exception e) {
                return null;
            }

            // Convert string to object
            try {
                jsonObject = new JSONObject(result);
            } catch(JSONException e) {
                return null;
            }


            try {
                JSONObject response = ((JSONObject)jsonObject.get("response"));
                max = ((Integer)response.get("numFound"));
                JSONArray docs = ((JSONArray)response.get("docs"));
                if(docs.length()==0)
                {
                    pd.dismiss();
                    displayMessage d = new displayMessage();
                    d.message = "Sorry no results indexed for this term :( We are searching now..Try Again in a whilee";
                    EditText query  = (EditText) findViewById(R.id.query);
                    String queryText = query.getText().toString();
                    HttpClient httpclient = new DefaultHttpClient(); // for port 80 requests!
                    String dynamicURL = "http://54.187.40.103:82/dynamicCrawl.php?query="+queryText;
                    HttpGet httppost = new HttpGet(dynamicURL);
                    HttpResponse response2 = httpclient.execute(httppost);

                    MainActivity.this.runOnUiThread(d);
                }
                for(int i=0;i<docs.length();i++)
                {

                   JSONObject doc = (JSONObject)docs.get(i);
                    String url = doc.getString("stanresImage");
                    Bitmap a = a = getBitmapFromURL(url);
                 // ImageView image =  (ImageView) findViewById(R.id.imageView);

                   doc.put("stanresImage",a);
                UpdateUI update = new UpdateUI();
                update.doc = doc;
                MainActivity.this.runOnUiThread(update);

                }



        } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return "";
        }



        public Bitmap getBitmapFromURL(String src) {
            try {
                Log.e("src",src);
                URL url = new URL(src);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                Log.e("Bitmap","returned");
                return myBitmap;
            } catch (IOException e) {
                e.printStackTrace();
                Log.e("Exception",e.getMessage());
                return null;
            }
        }
        protected void onPreExecute()
        {
            pd=ProgressDialog.show(MainActivity.this,"","Please Wait",false);
        }
        protected void onPostExecute(String result) {
            pd.dismiss();

            LinearLayout images = (LinearLayout) findViewById(R.id.linear);
            Button Next = new Button(MainActivity.this);
            Next.setText("Next");
            Button Back = new Button(MainActivity.this);
            Back.setText("Back");
            images.addView(Next);
            images.addView(Back);
            Next.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    nextPage();
                }
            });
            Back.setOnClickListener(new Button.OnClickListener() {
                public void onClick(View v) {
                    previousPage();
                }
            });



        }

    } // end CallAPI


}
