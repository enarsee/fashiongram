

var globalLocation="";
var dataArray = "";
var dynamicCrawl=0;
lastStart=0;
totalPhotos=0;

//Geo Location HTML5
navigator.geolocation.getCurrentPosition(setLocation);

function setLocation(position)
{
	globalLocation = position.coords.latitude+","+position.coords.longitude;
	console.log(globalLocation);
}



//Onclick and keyboard handlers to call REST Service
$("button#searchButton").click(function(){	
	execute();
});

$('#queryText').on("keypress", function(e) {
        if (e.keyCode == 13) {
           execute();
        }       
});


//Get data and pass on to function to call API
function execute(){
var queryText = $("#queryText").val();

	searchHandler(queryText,globalLocation);

}



//Makes the AJAX Call
function searchHandler(query,location)
{

var loadUrl = generateQuery(query,location,lastStart);

$("#images").html("");
$("#pleasewait").show("slow");
	$.ajax({
            type: "GET",
            url: "http://54.187.40.103:8983/solr/select/",
            data: loadUrl,
            dataType: 'jsonp',
            crossDomain: true,
            jsonp: 'json.wrf',
            success: function (data) { parseData(data); },
            failure: function (errMsg) {
                alert(errMsg);
            }
        });
} 

//Form the Solr Query
function generateQuery(query,location,start)
{ 
	var queryArray = query.split(" ");
	var queryTerm = "q={boost b=recip(geodist(),1,1000)}";
	for(i=0;i<queryArray.length;i++)
	{
		queryTerm = queryTerm + "tags:*"+queryArray[i]+"*AND+captionText:*"+queryArray[i]+"*AND+";
	}
	queryTerm = queryTerm.substring(0,queryTerm.length-4);
	

	var loadUrl = "start="+start+"&wt=json&"+queryTerm+"&rows=9&sfield=location&pt="+location+"&sort=boostScore desc,captionTime desc&json.wrf=?";
	return loadUrl;
}


function parseData(data)
{
	dataArray = data.response.docs;
	totalPhotos = data.response.numFound;

	console.log(dataArray.length);

	var totalLength = dataArray.length;
	
	var pageDisplay = 6; 

	if(typeof dataArray[0]==="undefined")
			{
				$("#images").append("<center><img src='img/404.jpg'><br><br><p style='font-size:20px;'>No images found yet :( <br> We have started working on the query you just entered,<br> Refreshing in 10 seconds, to get new results :D . May take longer though.</p></center>");
				var queryText = $("#queryText").val();
				url = "http://54.187.40.103:82/dynamicCrawl.php?query="+queryText;
		if(dynamicCrawl==0)
		{
				$.ajax({
		
            type: "GET",
            cache: false,
            url: url,
            dataType: 'jsonp'           
        });
				dynamicCrawl = 1;
			}

				setTimeout(function() { execute(); }, 10000);
			}


	else
		updateUI(dataArray);	

}


//Updates on UI
function updateUI(iData)
{
	$("#images").html("");
	for(i=0;i<dataArray.length;i++)
		{	
			var loc = "";	

				var image = "<div class='image' ><div id='infoBox'><a href='http://instagram.com/"+iData[i].userName+"'><div id='userName'>"+iData[i].userName+" </a><image width='16' height='16' src='img/comment.png'> "+iData[i].commentCount+"</img>  <image width='16' height='16' src='img/like.png'> "+iData[i].likesCount+"</img></div></div><div id='caption'>"+iData[i].captionText+"<br>"+getLocation(iData[i].location)+"</div><div id='imageFrame'><a href='"+iData[i].link+"'><img width='290px' height='300px' src='"+iData[i].stanresImage+"' onerror='imgError(this);'></a></div></div>";

		$("#images").append(image);

		}

	var paginationDiv = "<center><div class='pagination' style='margin-top:20px;margin-bottom:20px;'><a href='#' onclick='previousPage()'><img src='img/back.png'</a>  <a href='#' onclick='nextPage()'><img src='img/front.png'></a></div>";
	$("#images").append(paginationDiv);
	$("#pleasewait").hide("slow");
}

//Function to reverse geocode
function getLocation(location)
{ 	var loc="";
	if(location!="0,0")
	{
	var url = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+location+"&sensor=false&key=AIzaSyBOcgvTAPn8PtuyquJ_lC_VTKId4Bvg6Hg";
	console.log(url);
	$.ajax({
            type: "GET",
            url: url,
            global:false,
            dataType: 'json',
            async:false,
            success: function (data) {            	
            	if(typeof data.results[0] === 'undefined')
            	{
            		loc = "Taken at "+location;
            	}
            	else
            		loc =  "Taken at "+data.results[0].formatted_address; 
            }
        });
		}
	return loc;
}

//Function to hide div if error in image
function imgError(element)
{
	var $parent = $(element).parent().parent();

$parent.hide();
}

function nextPage()
{
	var queryText = $("#queryText").val();

	lastStart = lastStart + 10;
	if((lastStart+10)<totalPhotos)
		searchHandler(queryText,globalLocation);
}

function previousPage()
{
	var queryText = $("#queryText").val();

	if(lastStart>=10)
	{

	lastStart = lastStart -10;

	searchHandler(queryText,globalLocation);
	}
}



