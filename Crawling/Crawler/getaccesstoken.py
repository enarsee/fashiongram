#!C:\Python27\python.exe
print "Content-Type: text/html"
print 

import cgi 
import cgitb; cgitb.enable() 

from instagram.client import InstagramAPI
import sys
import json as simplejson

if len(sys.argv) > 1 and sys.argv[1] == 'local':
    try:
        from test_settings import *

        InstagramAPI.host = test_host
        InstagramAPI.base_path = test_base_path
        InstagramAPI.access_token_field = "access_token"
        InstagramAPI.authorize_url = test_authorize_url
        InstagramAPI.access_token_url = test_access_token_url
        InstagramAPI.protocol = test_protocol
    except Exception:
        pass

client_id = 'a1f4b199d48848a9832562947af486d4'
client_secret = '3f39f7b0a87441c0a38513ef1d660b63'
redirect_uri = "http://localhost:81/cgi-bin/getaccesstoken.py"
raw_scope = ""
scope = raw_scope.split(' ')
# For basic, API seems to need to be set explicitly
if not scope or scope == [""]:
    scope = ["basic"]

api = InstagramAPI(client_id=client_id, client_secret=client_secret, redirect_uri=redirect_uri)
redirect_uri = api.get_authorize_login_url(scope = scope)
"""
print "Visit this page and authorize access in your browser:\n", redirect_uri
"""
code = 'f9af7917aeb24d36905912299da6bbd2'

access_token = api.exchange_code_for_access_token(code)
#access_token =api.exchange_for_access_token(self, code=code, username="prathnasukheja", password="bp761993", scope=scope, user_id="324533240")

print "access token:", access_token
