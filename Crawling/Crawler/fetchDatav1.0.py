import MySQLdb
from instagram.client import InstagramAPI
import sys
import urllib, json
from random import randint
import traceback

if len(sys.argv) > 1 and sys.argv[1] == 'local':
    try:
        from test_settings import *

        InstagramAPI.host = test_host
        InstagramAPI.base_path = test_base_path
        InstagramAPI.access_token_field = "access_token"
        InstagramAPI.authorize_url = test_authorize_url
        InstagramAPI.access_token_url = test_access_token_url
        InstagramAPI.protocol = test_protocol
    except Exception:
        pass

client_id = 'a1f4b199d48848a9832562947af486d4'
client_secret = '3f39f7b0a87441c0a38513ef1d660b63'
#redirect_uri = "http://localhost:81/cgi-bin/getaccesstoken.py"

raw_scope = ""
scope = raw_scope.split(' ')

# For basic, API seems to need to be set explicitly
if not scope or scope == [""]:
    scope = ["basic"]


access_token='324533240.a1f4b19.26e82a35d1434ebeaa77b0719947e759'                
#print "access token:", access_token

#Crawling by tags, look for recent media which has the brand name as a tag
brand1=["forever21","charlesandkeith","hnm","bershka","gap","abercrombieandfitch","aeropostale","levis","elle","zara"]
#Crawling by userId of the above brands, look for recent media uploaded by the brand profile on instagram
brand2=["11707579","176895875","23410080","20829767","9456013","18682639","42583155","4641204","178598981","189957576"]

#Connect to  sql database
try:
	db = MySQLdb.connect(charset='utf8',host="localhost", user="root",passwd="wampmysql", db="instagram_data") 
except Exception as e:
	print e
# you must create a Cursor object
cur = db.cursor() 


row_count=0
item_count=0

#Iterating through all brands
for k in range(5,6):
	url="https://api.instagram.com/v1/tags/"+brand1[k]+"/media/recent?access_token="+access_token		
	#Crawling both urls
	for i in range(0,2):
		#Each search returns 20 items so crawling it 40 times to get 800 items per url
		for j in range(0,40):
			response = urllib.urlopen(url)
			json_data = json.loads(response.read())
		
			for item in json_data["data"]:
				item_count=item_count+1
				print "item_count:",item_count
				try:
			
					#Getting tags of the image and seperating them by comma
					Tags=' '.join(item['tags'])
					Tags = Tags.decode('utf8')
					#Data type i.e image or video
					data_type=item['type']
					#location from where image was uploaded
					if(item['location']):
						if(item['location']['longitude']): 
							location=str(item['location']['latitude'])+","+str(item['location']['longitude'])
						else:
							location="1000,1000"
					else:
						location="1000,1000"
					#Comment count
					commentCount=item['comments']['count']
					#Likes count
					likesCount=item['likes']['count']
					#image urls
					lowres_image=item['images']['low_resolution']['url']
					standardres_image=item['images']['standard_resolution']['url']
					thumbnail_image=item['images']['thumbnail']['url']

					#image link
					link = item['link']
					#image caption information
					caption_time=item['created_time']
					try:
						caption_text=item['caption']['text'].decode('utf8') if item['caption'] else "None"
					except:
						caption_text="None"
						print "in except"
					#image user information
					user_name=item['user']['username']
					user_id=item['user']['id']
					user_profile=item['user']['profile_picture']

					d1=Tags
					d2=data_type
					d3=location
					d4=commentCount
					d5=likesCount
					d6=lowres_image
					d7=standardres_image
					d8=thumbnail_image
					d9=caption_time
					d10=(caption_text)
					d11=user_name
					d12=user_id
					d13=user_profile
					d14=link
					"""
					print "Tags:",d1
					print "DT",d2
					print "Location",d3
					print "CC",d4
					print "likesCount",d5
					print "LR",d6
					print "SR",d7
					print "TI",d8
					print "Captime",d9
					"""
					#print "Captext",d10
					"""
					print "USername",d11
					print "user id",d12
					"""
					
					query="INSERT INTO jsondata(tags,dataType,location,commentCount,likesCount,lowresImage,stanresImage,thumbnailImage,captionTime,captionText,userName,userId,profilePicture,link)\
					VALUES('%s','%s','%s','%d','%d','%s','%s','%s','%s','%s','%s','%s','%s','%s')"%(d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11,d12,d13,d14)


					cur.execute(query)
					db.commit()
					row_count=row_count+1

					print "Row Count:",row_count
				except Exception as e:
					 traceback.print_exc()
			#Go to the next 20 items		
			url=json_data["pagination"]["next_url"]
		url="https://api.instagram.com/v1/users/"+brand2[k]+"/media/recent?access_token="+access_token
				



db.close()